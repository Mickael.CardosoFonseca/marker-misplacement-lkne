function MM = MIS_PlotPolar(m_RMSD_pMagn, Error)
HipFlex_1 = vec2mat(m_RMSD_pMagn(:,1),length(Error));
HipFlex_1 = [HipFlex_1(:,1) fliplr(HipFlex_1(:,2:8))];
HipAdd_1  = vec2mat(m_RMSD_pMagn(:,2),length(Error));
HipAdd_1 = [HipAdd_1(:,1) fliplr(HipAdd_1(:,2:8))];
HipRot_1  = vec2mat(m_RMSD_pMagn(:,3),length(Error));
HipRot_1 = [HipRot_1(:,1) fliplr(HipRot_1(:,2:8))];

KneeFlex_1 = vec2mat(m_RMSD_pMagn(:,4),length(Error));
KneeFlex_1 = [KneeFlex_1(:,1) fliplr(KneeFlex_1(:,2:8))];
KneeAdd_1  = vec2mat(m_RMSD_pMagn(:,5),length(Error));
KneeAdd_1 = [KneeAdd_1(:,1) fliplr(KneeAdd_1(:,2:8))];
KneeRot_1  = vec2mat(m_RMSD_pMagn(:,6),length(Error));
KneeRot_1 = [KneeRot_1(:,1) fliplr(KneeRot_1(:,2:8))];

AnkleFlex_1 = vec2mat(m_RMSD_pMagn(:,7),length(Error));
AnkleFlex_1 = [AnkleFlex_1(:,1) fliplr(AnkleFlex_1(:,2:8))];
AnkleAdd_1  = vec2mat(m_RMSD_pMagn(:,8),length(Error));
AnkleAdd_1 = [AnkleAdd_1(:,1) fliplr(AnkleAdd_1(:,2:8))];
AnkleRot_1  = vec2mat(m_RMSD_pMagn(:,9),length(Error));
AnkleRot_1 = [AnkleRot_1(:,1) fliplr(AnkleRot_1(:,2:8))];

% FootProg_1=vec2mat()

Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;
% Plot
subplot(3,3,1)
MIS_Radar_Plot(HipFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Flexion-Extension')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

subplot(3,3,2)
MIS_Radar_Plot(HipAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Adduction-Abduction')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

sbp = subplot(3,3,3)
MIS_Radar_Plot(HipRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Internal-External Rotation')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])
l=legend('5 mm', '10 mm', '15 mm', '20 mm', '30 mm');

subplot(3,3,4)
MIS_Radar_Plot(KneeFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Flexion-Extension')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

subplot(3,3,5)
MIS_Radar_Plot(KneeAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Adduction-Abduction')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

subplot(3,3,6)
MIS_Radar_Plot(KneeRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Internal-External Rotation')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

subplot(3,3,7)
MIS_Radar_Plot(AnkleFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Flexion-Extension')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

subplot(3,3,8)
MIS_Radar_Plot(AnkleAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Adduction-Abduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])

subplot(3,3,9)
MIS_Radar_Plot(AnkleRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Internal-External Rotation')
t.FontSize = 25
set(t,'position',get(t,'position')-[0 -0.5 0])

saveas(figure(1), 'LKNE.pdf')

% figure(2)
% plot()
end